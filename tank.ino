#include "defaults.h";

unsigned long int f_echo;
unsigned long int b_echo;

enum MOVEMENT {
  FORWARD,
  BACKWARD,
  TURNLEFT,
  TURNRIGHT,
  SPINLEFT,
  SPINRIGHT,
  STOP,
};

void setup(){
  pinMode(Echo_FSensor, INPUT );
  pinMode(Trig_FSensor, OUTPUT);
  pinMode(Echo_BSensor, INPUT );
  pinMode(Trig_BSensor, OUTPUT);
  pinMode(ENA_L298N  , OUTPUT);
  pinMode(IN1_L298N  , OUTPUT);
  pinMode(IN2_L298N  , OUTPUT);
  pinMode(IN3_L298N  , OUTPUT);
  pinMode(IN4_L298N  , OUTPUT);
  pinMode(ENB_L298N  , OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(KILLSWITCH , INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(Echo_FSensor), f_echo_triggered, FALLING);
  attachInterrupt(digitalPinToInterrupt(Echo_BSensor), b_echo_triggered, FALLING);
  randomSeed(analogRead(A5));
}

void loop(){
  unsigned long at = millis();
  bool button = digitalRead(KILLSWITCH);
  bool kill = fsm_killswitch(at,button);
  digitalWrite(LED_BUILTIN, kill);
  float f_delta, b_delta;
  fsm_sensors(&f_delta, &b_delta);
  float f_distance = distance_sound(f_delta,-0.07);
  float b_distance = distance_sound(b_delta,-0.40);
  fsm_behaviour(kill,at,f_distance,b_distance);
}

/* Function called by the interrupt */
void f_echo_triggered(){
  f_echo = micros();
}
/* Function called by the interrupt */
void b_echo_triggered(){
  b_echo = micros();
}

/* FSM that toggles the killswitch when a button is pressed */
bool fsm_killswitch(unsigned long at, bool button){
  static int state = 0;
  static int unsigned pt    = 0;
  switch (state){
    case 0: if (button) { 
              state += 1; return true; 
            }
            return false;
    case 1: if ( !button && at-pt >= T ) { 
              state += 1; 
              pt = at; 
            }
            return true;
    case 2: if (button) { state += 1; return false; }
            return true;
    case 3: if (!button && at-pt >= T ) { state = 0; pt = at; }
            return false;
  }
}

/* Function to control the Motors */
void motor_control(byte enb, byte ena, bool in3, bool in4, bool in2, bool in1){
  analogWrite(ENA_L298N, ena);
  analogWrite(ENB_L298N, enb);
  digitalWrite(IN1_L298N, in1);
  digitalWrite(IN2_L298N, in2);
  digitalWrite(IN3_L298N, in3);
  digitalWrite(IN4_L298N, in4);
}


/* Function that defines types of movement */
void movement(bool kill, byte speed, MOVEMENT m){
  if (kill) m = STOP;
  switch(m){
    case FORWARD   : motor_control(speed,speed,1,0,1,0); break;
    case BACKWARD  : motor_control(speed,speed,0,1,0,1); break;
    case TURNLEFT  : motor_control(speed,speed,1,0,0,0); break;
    case TURNRIGHT : motor_control(speed,speed,0,0,1,0); break;
    case SPINLEFT  : motor_control(speed,speed,1,0,0,1); break;
    case SPINRIGHT : motor_control(speed,speed,0,1,1,0); break;
    case STOP      : motor_control(0    ,0    ,0,0,0,0); break;
    default        : motor_control(0    ,0    ,0,0,0,0);
  }
  Serial.print(" Movement: ");
  Serial.print(m);
}

enum SENSOR_ACTION {
  ECHO,
  TRIGGER
};

/* FSM that controls both sensors */
/* Not very reusable but it does the job */
void fsm_sensors(float * f_delta, float * b_delta){
  static SENSOR_ACTION state = TRIGGER;
  static unsigned long trig = 0;
  static unsigned long local_b_delta = 0;
  static unsigned long local_f_delta = 0;
  switch (state){
    case TRIGGER: state = ECHO;
                  digitalWrite(Trig_FSensor, LOW); 
                  digitalWrite(Trig_BSensor, LOW);
                  delayMicroseconds(2);
                  digitalWrite(Trig_FSensor, HIGH); 
                  digitalWrite(Trig_BSensor, HIGH);
                  delayMicroseconds(5);
                  digitalWrite(Trig_FSensor, LOW);
                  digitalWrite(Trig_BSensor, LOW);
                  trig = micros();
                  f_echo = trig;
                  b_echo = trig;
                  break;
    case ECHO   : 
                  local_f_delta = f_echo - trig;
                  local_b_delta = b_echo - trig;
                  if ( local_f_delta > 0 && local_b_delta > 0 ){
                    *f_delta = local_f_delta;
                    *b_delta = local_b_delta;
                    state = TRIGGER;
                  }
                  break;
  }
}


/* Function to determine the distance from the sensor to an obstacle */
float distance_sound(unsigned long delta_t, float calibration_factor){
  return ( delta_t / (1e6) * 340 )/2+calibration_factor;
}

enum BEHAVIOUR {
  CHARGE,
  FREEZE,
  ROLLBACK,
  ADJUST
};

/* FSM that defines the tank behaviour */
void fsm_behaviour(bool kill, unsigned long at, float f_distance, float b_distance){
  static unsigned long pt;
  static BEHAVIOUR state = CHARGE;
  float min_dist = 0.1;
  static byte adjust_time;
  static byte spin_set = 0;
  static unsigned long spin_direction;
  static unsigned long spin_time;

  switch(state){
    case CHARGE   : if (f_distance <= min_dist ){ 
                      pt = at;
                      if (b_distance <= min_dist) state = ADJUST;
                      else                        state = ROLLBACK;
                    }
                    movement(kill,245,FORWARD);
                    break;

    case ROLLBACK : if (at-pt < rollback_time && b_distance > min_dist) movement(kill,225,BACKWARD); 
                    else {
                      state = ADJUST;
                      spin_time = random (spin_time_min,spin_time_max);
                      pt = at;
                    }
                    break;

    case ADJUST   : if (at-pt < spin_time){
                      if (spin_set == 0){
                        spin_direction = random (0,2);
                        spin_set = 1;
                      }
                      if (spin_direction == 0) movement(kill,255,SPINRIGHT);
                      else                     movement(kill,255,SPINLEFT);
                    }
                    else {
                      spin_set = 0;
                      state = FREEZE;
                      pt = at;
                    }
                    break;

    case FREEZE   : if (at-pt < freeze_time) movement(kill,0,STOP);
                    else{
                      if (f_distance <= min_dist) {
                        state = ADJUST;
                        spin_time = random (spin_time_min,spin_time_max);
                      }
                      else {
                        state = CHARGE;
                        pt = at;
                      }
                    }
                    break;

    default       : state = FREEZE;
  }
}
