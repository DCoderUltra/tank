const int Trig_BSensor = 11;
const int Echo_BSensor = 2;
const int Echo_FSensor = 3;
const int Trig_FSensor = 4;
const int ENA_L298N   = 6;
const int IN1_L298N   = 7;
const int IN2_L298N   = 8;
const int ENB_L298N   = 5;
const int IN3_L298N   = 9;
const int IN4_L298N   = 10;
const int KILLSWITCH  = 12;

const int T = 0.1;

const unsigned long TRIG_TIME = 0.1;
const unsigned long spin_time_min = 1000;
const unsigned long spin_time_max = 3000;
const unsigned long rollback_time = 500;
const unsigned long freeze_time = 100;
