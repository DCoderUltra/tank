![Arduino](https://img.shields.io/badge/Arduino-00878F?logo=arduino)
![C](https://img.shields.io/badge/Language-A8B9CC?logo=c&&logoColor=grey&&logoSize=auto)
![Version](https://img.shields.io/badge/Version-1.0-green)

# Target

Arduino code to control a Toy Tank with the help off: 

- Arduino UNO R3
- HC-SR04 2x (Ultrassonic Sensor)
- L298N (DC Motor driver module)
